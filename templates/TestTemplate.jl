include("../src/ContinuedFractions.jl")

using Test
using .cf


function parse_eval_dict(s::AbstractString, locals::Dict{})
    ex = Meta.parse(s)
    assignments = [:($sym::Rational{BigInt} = $val) for (sym,val) in locals]
    eval(:(let $(assignments...); $ex; end))
end

@testset "Tests" begin

    {{#:test_cases}}
    @testset "{{benchmark_name}}" begin   
        println("{{benchmark_name}}")

        d = Dict()
        {{#numbers}}
            {{name}} = cf.ContinuedFraction({{num}},{{den}})
            push!(d, Symbol("{{name}}") => {{num}}//{{den}})
            println("{{name}}", " = ",{{num}}," / ",{{den}})
        {{/numbers}}
            result = cf.cf_to_rational({{{expression}}})
            println("Expression = ", "{{{expression}}}")
            println(" ")

        result2 = parse_eval_dict("{{{expression}}}",d)

        @test result == result2
    end
    {{/:test_cases}}

end
