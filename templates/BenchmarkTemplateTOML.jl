include("../src/ContinuedFractions.jl")

using BenchmarkTools
using .cf

{{#:test_cases}}
{{#numbers}}
{{name}} = cf.ContinuedFraction({{num}},{{den}})
{{/numbers}}

println("[[benchmarkResults]]")
println("benchmark_name = \"{{benchmark_name}}\"")

compute_time = median(@benchmark cf.compute({{{expressionb}}}))
convert_time = median(@benchmark cf.cf_to_rational({{{expressionb}}}))

println("cf_compute_time = ",compute_time.time)
println("cf_compute_gctime = ",compute_time.gctime)
println("cf_compute_memory = ",compute_time.memory)
println("cf_compute_allocs = ",compute_time.allocs)

println("cf_convert_time = ",convert_time.time)
println("cf_convert_gctime = ",convert_time.gctime)
println("cf_convert_memory = ",convert_time.memory)
println("cf_convert_allocs = ",convert_time.allocs)

{{#numbers}}
{{name}} = Rational{BigInt}({{num}},{{den}})
{{/numbers}}

compute_time_gmp = median(@benchmark {{{expressionb}}})

println("gmp_compute_time = ",compute_time_gmp.time)
println("gmp_compute_gctime = ",compute_time_gmp.gctime)
println("gmp_compute_memory = ",compute_time_gmp.memory)
println("gmp_compute_allocs = ",compute_time_gmp.allocs)

ratio_cfcompute_gmp = ratio(compute_time,compute_time_gmp)
println("time_ratio_cfcompute_gmp = ",ratio_cfcompute_gmp.time)
println("gctime_ratio_cfcompute_gmp = ",ratio_cfcompute_gmp.gctime)
println("memory_ratio_cfcompute_gmp = ",ratio_cfcompute_gmp.memory)
println("allocs_ratio_cfcompute_gmp = ",ratio_cfcompute_gmp.allocs)

ratio_cfconvert_gmp = ratio(convert_time,compute_time_gmp)
println("time_ratio_cfconvert_gmp = ",ratio_cfconvert_gmp.time)
println("gctime_ratio_cfconvert_gmp = ",ratio_cfconvert_gmp.gctime)
println("memory_ratio_cfconvert_gmp = ",ratio_cfconvert_gmp.memory)
println("allocs_ratio_cfconvert_gmp = ",ratio_cfconvert_gmp.allocs)
println(" ")

{{/:test_cases}}
