include("./src/ContinuedFractions.jl")

using .cf
T = Int64
obj = cf.ContinuedFraction([T(2),T(3),T(2)]) # array definition
obj1 = cf.ContinuedFraction(T(3),T(2)) # rational definition
obj2 = cf.ContinuedFraction(T(3)) # just number definition

# you can reuse cf
a = obj + obj1
b = obj + obj1

cf.compute(a)
cf.compute(b)
print(a,"\n")
print(b,"\n")



# you can use cf in the loop 
while a.hasNext
        ready, convergent = cf.next(a)
        if ready
            print("^",convergent,"^\n")
        end
end


# get rational equivalent of cf
num,den = cf.cf_to_rational(obj)
print(num,"/",den)
