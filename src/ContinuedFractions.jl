module cf

include("./Exception.jl")

mutable struct ContinuedFraction{T<:Integer}
    a::T
    b::T
    c::T
    d::T
    e::T
    f::T
    g::T
    h::T

    x::Union{Nothing,ContinuedFraction} # first number
    y::Union{Nothing,ContinuedFraction} # second number

    indexX::Int # iterator order for x
    indexY::Int # iterator order for y
    indexItself::Int #iterator for itself

    cList::Vector{T} # coefficient list

    ready::Bool   # ready for output or not
    hasNext::Bool # has a next coefficient or nnot
end


function ContinuedFraction(a::T)    where {T<:Integer} # one coefficient case, it is pushed to cList
    return ContinuedFraction{T}(T(0),T(0),T(0),T(0),T(0),T(0),T(0),T(0),nothing,nothing,1,1,1,[T(a)],true,true)
end

function ContinuedFraction(a::T,e::T) where {T<:Integer} # nominator & denominator case
    (e == 1) ? (return ContinuedFraction(a)) : (return ContinuedFraction{T}(T(a),T(0),T(0),T(0),T(e),T(0),T(0),T(0),nothing,nothing,1,1,1,[],false,true))
end

function ContinuedFraction(list::Vector{T}) where {T<:Integer} # multiple coefficient case , values are coppied to cList
    return ContinuedFraction{T}(T(0),T(0),T(0),T(0),T(0),T(0),T(0),T(0),nothing,nothing,1,1,1,list,true,true)
end

function ContinuedFraction(a::Integer,e::Integer) # to fix different types (i.e. numaretor(Int64) & denominator(BigInt)) 
    T = promote_type(typeof(a),typeof(e)) # finds common type of them
    return ContinuedFraction(T(a),T(e))
end

# overloading sum
function Base.:+(x::ContinuedFraction{X}, y::ContinuedFraction{Y}) where {X,Y<:Integer}
    T = promote_type(X,Y)
    return ContinuedFraction{T}(T(0),T(1),T(1),T(0),T(1),T(0),T(0),T(0),x,y,1,1,1,[],false,true)
end

# overloading sub
function Base.:-(x::ContinuedFraction{X}, y::ContinuedFraction{Y}) where {X,Y<:Integer}
    T = promote_type(X,Y)
    return ContinuedFraction{T}(T(0),T(1),T(-1),T(0),T(1),T(0),T(0),T(0),x,y,1,1,1,[],false,true)
end

# overloading mul
function Base.:*(x::ContinuedFraction{X}, y::ContinuedFraction{Y}) where {X,Y<:Integer}
    T = promote_type(X,Y)
    return ContinuedFraction{T}(T(0),T(0),T(0),T(1),T(1),T(0),T(0),T(0),x,y,1,1,1,[],false,true)
end

# overloading div
function Base.:/(x::ContinuedFraction{X}, y::ContinuedFraction{Y}) where {X,Y<:Integer}
    T = promote_type(X,Y)
    return ContinuedFraction{T}(T(0),T(1),T(0),T(0),T(0),T(0),T(1),T(0),x,y,1,1,1,[],false,true)
end

# overloading power(power takes number only not cf)
function Base.:^(x::ContinuedFraction{X}, y::Integer) where {X<:Integer}
    mul = cf.ContinuedFraction(1)

    for i in 1:y 
        mul = x*mul
    end

    return mul
end

function next(cf_number::ContinuedFraction)
 
    convergent,cf_number.indexItself = iterate(cf_number,cf_number.indexItself)
    
    if convergent==nothing
        cf_number.indexItself = 1
        cf_number.hasNext = false
        return false,nothing #false means there is no more next element. Initialize list index to 1 to reuse number
    end
    return true,convergent
end

function iterate(cf_number::ContinuedFraction,index::Int)
    while index > length(cf_number.cList)
        if !compute_next_convergent(cf_number)
            return nothing,index
        end
    end
    return cf_number.cList[index],index+1
end

function compute_next_convergent(cf_number::ContinuedFraction)

    if cf_number.ready
        return false
    end

    while true

        if (cf_number.e == 0) & (cf_number.f == 0) & (cf_number.g == 0) & (cf_number.h == 0)
            break
        end

        if  (cf_number.x === nothing) & (cf_number.y === nothing)
            break
        end

      r1 =( cf_number.e==0 ? ( cf_number.a==0 ? NaN : Inf ) : fld(cf_number.a,cf_number.e) )
      r2 =( cf_number.f==0 ? ( cf_number.b==0 ? NaN : Inf ) : fld(cf_number.b,cf_number.f) )
      r3 =( cf_number.g==0 ? ( cf_number.c==0 ? NaN : Inf ) : fld(cf_number.c,cf_number.g) )
      r4 =( cf_number.h==0 ? ( cf_number.d==0 ? NaN : Inf ) : fld(cf_number.d,cf_number.h) )

      # Ready to egest
      if r1 == r2 == r3 == r4
        break
      end


        if !( (cf_number.e == 0) | (cf_number.f == 0) | (cf_number.g == 0)) ? (abs(r2-r1) > abs(r3-r1)) : (((cf_number.e == 0) & (cf_number.f != 0) & (cf_number.g == 0)) | ((cf_number.e != 0) & (cf_number.f == 0) & (cf_number.g != 0)))
            # X chosen 
            if (cf_number.x === nothing) | (cf_number.x.ready & (cf_number.indexX > length(cf_number.x.cList))) # no more convergent in X or there is not X
                 cf_number.a = cf_number.b
                 cf_number.c = cf_number.d
                 cf_number.e = cf_number.f
                 cf_number.g = cf_number.h
            else # ingest from X
               
                    p , cf_number.indexX = iterate(cf_number.x,cf_number.indexX)
                    if p === nothing  continue end
                     mb = cf_number.a + cf_number.b * p
                     md = cf_number.c + cf_number.d * p
                     mf = cf_number.e + cf_number.f * p
                     mh = cf_number.g + cf_number.h * p

                     cf_number.a,cf_number.b,cf_number.c,cf_number.d   = cf_number.b,mb,cf_number.d,md
                     cf_number.e,cf_number.f,cf_number.g,cf_number.h = cf_number.f,mf,cf_number.h,mh
            end
        else
            # Y chosen
            if (cf_number.y === nothing) | (cf_number.y.ready & (cf_number.indexY > length(cf_number.y.cList)))
                 cf_number.a = cf_number.c
                 cf_number.b = cf_number.d
                 cf_number.e = cf_number.g
                 cf_number.f = cf_number.h
            else #ingest from Y
              
                    q , cf_number.indexY = iterate(cf_number.y,cf_number.indexY)
                    if q === nothing  continue end
                    mc = cf_number.a + cf_number.c * q
                    md = cf_number.b + cf_number.d * q
                    mg = cf_number.e + cf_number.g * q
                    mh = cf_number.f + cf_number.h * q

                    cf_number.a,cf_number.b,cf_number.c,cf_number.d  = cf_number.c,cf_number.d,mc,md
                    cf_number.e,cf_number.f,cf_number.g,cf_number.h = cf_number.g,cf_number.h,mg,mh
            end
        end
    end

    if cf_number.e != 0 #egestion part
        r = fld(cf_number.a,cf_number.e)
        push!(cf_number.cList,r)


        new_e = cf_number.a - cf_number.e *r 
        new_f = cf_number.b - cf_number.f *r  
        new_g = cf_number.c - cf_number.g *r 
        new_h = cf_number.d - cf_number.h *r 
        

        cf_number.a,cf_number.b,cf_number.c,cf_number.d  = cf_number.e,cf_number.f,cf_number.g,cf_number.h 
        cf_number.e,cf_number.f,cf_number.g,cf_number.h = new_e,new_f,new_g,new_h
    end

    if (cf_number.e == 0) & (cf_number.f == 0) & (cf_number.g == 0) & (cf_number.h == 0)
        cf_number.ready = true
    end

    return true
end

function compute(cf_number::ContinuedFraction)
    while !cf_number.ready
        compute_next_convergent(cf_number)
    end
end

function cf_to_rational(cf_number::ContinuedFraction)

    num_new = 1
    num_old = 0
    den_new = 0
    den_old = 1

    while !cf_number.ready
        compute_next_convergent(cf_number)
    end
    
    for index in 1:length(cf_number.cList)
        num_tmp = num_new
        den_tmp = den_new
        num_new = (cf_number.cList[index] * num_new) + num_old
        den_new = (cf_number.cList[index] * den_new) + den_old
        num_old = num_tmp
        den_old = den_tmp
        
    end

    return num_new//den_new

end



end # end module
