using Mustache
using TOML

io = open("../examples/input.toml", "r")
inside_file = TOML.parse(io)
close(io)

test_cases = inside_file["benchmarks"]

test_template = read("../templates/TestTemplate.jl", String)
bechmark_template = read("../templates/BenchmarkTemplate.jl", String)
bechmark_template_TOML = read("../templates/BenchmarkTemplateTOML.jl", String)

io = open("./Tests.jl","w")
write(io, render(test_template, test_cases=test_cases))
close(io)

io = open("../benchmark/Benchmarks.jl","w")
write(io, render(bechmark_template, test_cases=test_cases))
close(io)

io = open("../benchmark/BenchmarksTOML.jl","w")
write(io, render(bechmark_template_TOML, test_cases=test_cases))
close(io)
