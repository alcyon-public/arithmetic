julia GenerateTestsBenchmarks.jl # generates Tests.jl and Benchmarks.jl
julia Tests.jl                   # runs tests
julia ../benchmark/BenchmarksTOML.jl > ../benchmark/results/result.toml # runs benchmarks , puts results to toml file

